# RogueSensei
The only thing greater than computers and code is a cup of tea. I drink tea and I code things.

My favourite language is Rust; I also like C/C++, C# and Python.
## My personal projects
- [Wilson](https://gitlab.com/roguesensei/rusty-wilson.git) - My discord bot written in Rust
    - Also see the original [Python Version](https://gitlab.com/roguesensei/wilson.git)
- [CLogger](https://gitlab.com/roguesensei/clogger.git) - A tiny logging library written in C
- More to be published soon.

Consider inviting Wilson to your discord server by clicking [here](https://discord.com/oauth2/authorize?client_id=370210465672069122).

Thanks for stopping by, happy hacking!
